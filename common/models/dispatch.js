'use strict';
const PolygonMod = require("./common/Polygon");
const polygon = new PolygonMod("pointInPolygon")
const distance = require('google-distance');
const config = require("../../config");    //require("./config")
module.exports = function (Dispatch) {
	var driverInfo = [];
	var polygonInfo = [];
	var finalResult = { "isAvailable": false }

	//----------------------------quotes----------------------------------------------------//
	Dispatch.remoteMethod(
		'quotes',
		{
			description: 'To get order summary data',
			http: { verb: 'post' },
			accepts: [
				{ arg: 'data', type: 'object', http: { source: 'body' } }
			],
			returns: { type: 'array', root: true }
		}
	);

	Dispatch.quotes = function (data, cb) {
		//check both addresses is available on one polygon.
		if (typeof data["pickup"]["latitude"] != undefined && typeof data["pickup"]["longitude"] != undefined && typeof data["dropoff"]["latitude"] != undefined && typeof data["dropoff"]["longitude"] != undefined) {
			//for check address
			Dispatch.checkAddresses(data, function (error, result) {
				//check address.
				if (result) {
					Dispatch.checkDriver(data, function (error, driverResult) {
						//check driver.
						if (driverResult) {
							finalResult["isAvailable"] = true;
							Dispatch.getQuote(data, function (error, driverResult) {
								cb(null, finalResult);
							});
						} else {
							finalResult["unavailableReason"] = "DriverNotAvailable";
							cb(null, finalResult);
						}
					});
				} else {
					finalResult["unavailableReason"] = "OutsideDeliveryArea";
					cb(null, finalResult);
				}
			});
		}

	};

	//check address for pickup and drop.
	Dispatch.checkAddresses = function (data, cb) {
		let sql = "SELECT * FROM `polygon` WHERE `is_deleted` ='N' and olo_enabled = 'Y'";
		Dispatch.dataSource.connector.query(sql, (err, results) => {
			let pickupLatLong = data["pickup"]["latitude"] + "," + data["pickup"]["longitude"];
			let dropLatLong = data["dropoff"]["latitude"] + "," + data["dropoff"]["longitude"];
			let pickupPolygon = [];
			let dropPolygon = [];
			let res = 0;
			if (results.length > 0) {
				results.forEach(function (element) {
					let pikpoly = polygon.pointInPolygon(pickupLatLong, element["points"]);
					if (pikpoly != "outside") {
						pickupPolygon.push(element["polygon_id"]);
					}
				});

				for (let element of results) {
					if (pickupPolygon.includes(element["polygon_id"])) {
						let droppoly = polygon.pointInPolygon(dropLatLong, element["points"]);
						if (droppoly != "outside") {
							dropPolygon.push(element["polygon_id"]);
							res = 1;
							polygonInfo = element;
						}
					}
					if (res) { break; }
				}
				if (res == 0) {
					polygonInfo = [];
				}
			}
			cb(err, res);
		});

	};

	//check driver for pickup and drop.
	Dispatch.checkDriver = function (data, cb) {
		let latitude = data["dropoff"]["latitude"];
		let longitude = data["dropoff"]["longitude"];
		let sql = 'SELECT Distinct dd.* from user dr join driver_device dd on dd.status = 1 and dd.driver_id = dr.user_id  left join (SELECT `value`as mile FROM settings WHERE `key` = "FIND_DRIVER_WITHIN_MILES") dm on 1   where dr.is_deleted = "N" and dr.is_login = "Y" and if(dd.is_fulltime = "N", dd.stripe_custom_id != "","1") and degrees(acos(sin(radians(' + latitude + ')) * sin(radians(dd.latitude)) + cos(radians(' + latitude + ')) * cos(radians(dd.latitude)) * cos(radians(' + longitude + ' - dd.longitude)))) * 60 * 1.1515 < dm.mile and dr.user_id not in (SELECT Distinct `driver_id` FROM `order_main` WHERE `order_status`  in("accepted","started","arrived","manager_accepted","manager_timeout","call_placed") and `driver_id` = dr.user_id)';
		Dispatch.dataSource.connector.query(sql, (err, results) => {
			let res = 0;
			if (results.length > 0) {
				res = 1;
				driverInfo = results[0];
			} else {
				driverInfo = [];
			}
			cb(err, res);

		});

	};

	//Get Quote.
	Dispatch.getQuote = function (data, cb) {
		let pickupLatLong = data["pickup"]["latitude"] + "," + data["pickup"]["longitude"];
		let dropLatLong = data["dropoff"]["latitude"] + "," + data["dropoff"]["longitude"];
		let driverLatLong = driverInfo["latitude"] + "," + driverInfo["longitude"];
		//estimatedPickupTime
		let pickupTime = {
			index: 1,
			origin: driverLatLong,
			destination: pickupLatLong
		};
		let dropoffTime = {
			index: 1,
			origin: pickupLatLong,
			destination: dropLatLong
		};
		distance.apiKey = config.googleKey;
		distance.get(pickupTime, function (err, _pickupTime) {
			distance.get(dropoffTime, function (err, _dropoffTime) {

				let currentTime, parsedDate, pickNewDate, dropNewDate, expirNewDate;
				currentTime = new Date();

				//for pickup time
				let pickupBufferTime = parseInt(config.pickupBufferTime) + parseInt(_pickupTime.durationValue);
				parsedDate = new Date(Date.parse(currentTime));
				pickNewDate = new Date(parsedDate.getTime() + (1000 * pickupBufferTime));

				//for dropoff time
				let dropBufferTime = parseInt(config.dropBufferTime) + parseInt(_dropoffTime.durationValue);
				parsedDate = new Date(Date.parse(pickNewDate));
				dropNewDate = new Date(parsedDate.getTime() + (1000 * dropBufferTime));

				//for expires time
				let expiresTime = config.expirTime;
				parsedDate = new Date(Date.parse(currentTime));
				expirNewDate = new Date(parsedDate.getTime() + (1000 * expiresTime));

				finalResult["estimatedPickupTime"] = pickNewDate;
				finalResult["estimatedDropoffTime"] = dropNewDate;
				finalResult["expires"] = expirNewDate;

				//for get delivery fee 
				let cityId = polygonInfo.city_id;
				var miles = parseInt(_dropoffTime.distanceValue) * 0.00062137;

				let sql = 'SELECT if(cdf.city_id IS NULL,df.non_partner_fee,cdf.non_partner_fee ) as delivery_fees, df.non_partner_fee,cdf.non_partner_fee, cdf.city_id FROM  delivery_fee df left join city_delivery_fee cdf on cdf.city_id = ' + cityId + ' and cdf.is_deleted = "N" WHERE  df.is_deleted = "N" and   if (cdf.city_id IS NULL, df.distance <= ' + miles + ', cdf.distance <= ' + miles + ')    ORDER BY if (cdf.city_id IS NULL, df.distance, cdf.distance) DESC LIMIT 1';

				Dispatch.dataSource.connector.query(sql, (err, results) => {
					finalResult["fee"] = 0;
					if (results.length > 0) {
						finalResult["fee"] = results[0]["delivery_fees"];
					}
					pickNewDate = pickNewDate.getTime() / 1000.0;
					dropNewDate = dropNewDate.getTime() / 1000.0;
					expirNewDate = expirNewDate.getTime() / 1000.0;
					//for insert data in database
					let sql = 'INSERT INTO `quotes`( `fee`, `estimatedPickupTime`, `estimatedDropoffTime`, `expires`,`polygon_id`,`miles`) VALUES (' + finalResult["fee"] + ',"' + pickNewDate + '","' + dropNewDate + '","' + expirNewDate + '","' + polygonInfo["polygon_id"] + '","' + miles + '")';

					Dispatch.dataSource.connector.query(sql, (err, results) => {
						finalResult["quoteId"] = 0;
						if (results.insertId > 0) {
							finalResult["quoteId"] = results.insertId;
						}
						cb(err, 1)
					});
					//for insert data in database


				});

				// cb(err,{
				// 	"currentTime": currentTime,
				// 	"pickupTime": pickNewDate,
				// 	"dropoffTime": dropNewDate,
				// 	"_dropoffTime": _dropoffTime,
				// 	"expiresTime": expirNewDate
				// });
			});
		});
	};

	//----------------------------quotes----------------------------------------------------//

	//----------------------------quotes/quoteId/accept-------------------------------------------------//
	Dispatch.remoteMethod(
		'acceptQuotes',
		{
			description: 'To accept order.',
			http: { path: '/quotes/:id/accept', verb: 'post' },
			accepts: [
				{ arg: 'id', type: 'string', http: { source: 'path' } },
				{ arg: 'data', type: 'object', http: { source: 'body' } }
			],
			returns: { type: 'array', root: true }
		}
	);

	Dispatch.acceptQuotes = function (id, data, cb) {
		var Quotes = Dispatch.app.models.Quotes;
		Quotes.find({ where: { "quoteid": id } }, function (err, quotes) {
			if (quotes.length > 0) {
				let tip = (typeof data["tip"] == undefined) ? 0 : data["tip"];
				let nd = new Date(Date.parse(new Date()) + (3600000 * (config.offset)));
				let parsedDate, pickupTime, createdDttm;
				parsedDate = new Date(Date.parse(new Date()));

				createdDttm = new Date(parsedDate.getTime());

				pickupTime = new Date(parsedDate.getTime());
				pickupTime = pickupTime.getTime() / 1000.0;

				let note = (typeof data["dropoff"]["instructions"] != undefined) ? data["dropoff"]["instructions"] : "";
				//for save order
				let orderData = {
					"mobileNumber": data["dropoff"]["phoneNumber"],
					"note": note,
					"fullAddress": data["dropoff"]["street"] + "," + data["dropoff"]["city"] + "," + data["dropoff"]["state"] + "," + data["dropoff"]["postalCode"] + "," + data["dropoff"]["unit"],
					"orderStatus": "placed",
					"tipType": "amt",
					"orderTip": tip,
					"tipValue": tip,
					"createdDttm": nd,
					"expectedDeliveryDttm": 44,
					"distance": "11	",
					"companyType": "OLO",
					"polygonId": "11",
					"pickupTime": pickupTime
				}
				var orderMain = Dispatch.app.models.order_main;
				orderMain.create(orderData, function (err2, _orderMain) {
					const orderId = _orderMain["orderId"];
					const all_data = JSON.stringify(data["orderDetails"], null, '\t');
					//for save order details.
					const OrderDetailsData = {
						"orderId": orderId,
						"allData": all_data
					}
					var OrderDetails = Dispatch.app.models.order_details;
					OrderDetails.create(OrderDetailsData, function (err2, _OrderDetails) {});
					//for save order details.

					//for save order Note.
					if (typeof data["pickup"]["instructions"] != undefined) {
						const orderNoteData = {
							"orderId": orderId,
							"noteFor": "olo_pickup",
							"note": data["pickup"]["instructions"],
							"createdDttm": nd
						}
						var orderNote = Dispatch.app.models.order_note;
						orderNote.create(orderNoteData, function (err2, _orderNote) {});
					}//for save order Note.
					
					//save to olo order main table
					const oloOrderMainData = {
						"orderId": orderId,
						"quoteId": quotes[0]["quoteid"],
						"pName": data["pickup"]["name"],
						"pPhoneNumber": data["pickup"]["phoneNumber"],
						"pStreet": data["pickup"]["street"],
						"pCity": data["pickup"]["city"],
						"pState": data["pickup"]["state"],
						"pPostalcode": data["pickup"]["postalCode"],
						"pLatitude": data["pickup"]["latitude"],
						"pLongitude": data["pickup"]["longitude"],
						"pUnit": data["pickup"]["unit"],
						"dName": data["dropoff"]["name"],
						"dPhoneNumber": data["dropoff"]["phoneNumber"],
						"dStreet": data["dropoff"]["street"],
						"dCity": data["dropoff"]["city"],
						"dState": data["dropoff"]["state"],
						"dPostalcode": data["dropoff"]["postalCode"],
						"dLatitude": data["dropoff"]["latitude"],
						"dLongitude": data["dropoff"]["longitude"],
						"dUnit": data["dropoff"]["unit"],
						"quoteExternalReference": data["quoteExternalReference"],
						"deliveryExternalReference": data["deliveryExternalReference"],
						"deliveryid": data["deliveryId"],
						"controlledContents": data["controlledContents"],
						"allowedVehicles": data["allowedVehicles"],
						"createdDttm": nd
					}

					var oloOrderMain = Dispatch.app.models.olo_order_main;
					oloOrderMain.create(oloOrderMainData, function (err2, _oloOrderMain) {
						cb(null, {
							"confirmationId": orderId
						});
					});//save to olo order main table 


				});//for save order
			} else {
				cb("quote not found", null)
			}

		});


	};
	//----------------------------quotes/quoteId/accept-------------------------------------------------//
	
};
