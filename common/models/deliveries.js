'use strict';

const distance = require('google-distance');
const config = require("../../config");

module.exports = function (Deliveries) {	
//----------------------------deliveries/deliveryId/status---------------------------------------//
	Deliveries.remoteMethod(
		'deliveryStatus',
		{
			description: 'for delivery status.',
			http: { path: '/:deliveryId/status', verb: 'post' },
			accepts: [
				{ arg: 'deliveryId', type: 'string', http: { source: 'path' } }
			],
			returns: { type: 'array', root: true }
		}
	);

	Deliveries.deliveryStatus = function (deliveryId, cb) {
		let sql = 'select om.order_id,om.order_status as "status" ,concat(dr.firstname," ",dr.lastname) as "driverName",dr.mobile_number as "driverPhoneNumber", "car" as "transportType",dd.latitude,dd.longitude,dd.latitude,dd.longitude,oom.p_latitude,oom.p_longitude,oom.d_latitude,oom.d_longitude from order_main om join olo_order_main oom on om.order_id = oom.order_id left join user dr on dr.user_id = om.driver_id left join driver_device dd on dd.driver_id = om.driver_id WHERE oom.deliveryId = "' + deliveryId +'"';
		var finalResult={};

		Deliveries.dataSource.connector.query(sql, (err, dataResults) => {
			if (dataResults.length > 0) {
				//cb(null, dataResults[0]["status"]);
				let _status = dataResults[0]["status"];
				finalResult["status"] = _status;
				if (dataResults[0]["driverName"] != null ){
					finalResult["driverName"] = dataResults[0]["driverName"];
					finalResult["driverPhoneNumber"] = dataResults[0]["driverPhoneNumber"];
					finalResult["transportType"] = dataResults[0]["transportType"];
				}
				if (dataResults[0]["driverName"] != null) {
					let pickupLatLong = dataResults["0"]["p_latitude"] + "," + dataResults["0"]["p_longitude"];
					let dropLatLong = dataResults["0"]["d_latitude"] + "," + dataResults["0"]["d_longitude"];
					let driverLatLong = dataResults["0"]["latitude"] + "," + dataResults["0"]["longitude"];
					//estimatedPickupTime
					let pickupTime = {
						index: 1,
						origin: driverLatLong,
						destination: pickupLatLong
					};
					let dropoffTime = {
						index: 1,
						origin: pickupLatLong,
						destination: dropLatLong
					};
					
					distance.apiKey = config.googleKey;
					distance.get(pickupTime, function (err, _pickupTime) {
						distance.get(dropoffTime, function (err, _dropoffTime) {
							
							let currentTime, parsedDate, pickNewDate, dropNewDate;
							currentTime = new Date();

							//for pickup time
							let pickupBufferTime = parseInt(config.pickupBufferTime) + parseInt(_pickupTime.durationValue);
							parsedDate = new Date(Date.parse(currentTime));
							pickNewDate = new Date(parsedDate.getTime() + (1000 * pickupBufferTime));

							//for dropoff time
							let dropBufferTime = parseInt(config.dropBufferTime) + parseInt(_dropoffTime.durationValue);
							parsedDate = new Date(Date.parse(pickNewDate));
							dropNewDate = new Date(parsedDate.getTime() + (1000 * dropBufferTime));
							
							finalResult["estimatedPickupTime"] = pickNewDate;
							finalResult["estimatedDropoffTime"] = dropNewDate;
							cb(null, finalResult);
						});
					});
				}else{
					cb(null, finalResult);
				}
				
				
			} else {
				cb({}, null);
			}

		});


	};
//----------------------------deliveries/deliveryId/status---------------------------------------//

//----------------------------/deliveries/deliveryId/cancel---------------------------------------//
	Deliveries.remoteMethod(
		'deliveriesCancel',
		{
			description: 'for delivery cancel.',
			http: { path: '/:deliveryId/cancel', verb: 'post' },
			accepts: [
				{ arg: 'deliveryId', type: 'string', http: { source: 'path' } },
				{ arg: 'data', type: 'object', http: { source: 'body' } }
			],
			returns: { type: 'array', root: true }
		}
	);

	Deliveries.deliveriesCancel = function (deliveryId,data, cb) {
		let sql = 'UPDATE `order_main` SET `order_status` = "cancelled" WHERE `order_id` =(SELECT `order_id` FROM `olo_order_main` WHERE `deliveryId` = "' + deliveryId +'" LIMIT 1)';
		
		Deliveries.dataSource.connector.query(sql, (err, dataResults) => {
			if (err) {
				cb(null, err);
			} else {
				if (dataResults["affectedRows"] > 0){
					//for update cancel reason.
					let oloOrderMainData = { "cancelReason": data["reason"] }
					let whereData = { "deliveryid": deliveryId }
	
					var oloOrderMain = Deliveries.app.models.olo_order_main;
					oloOrderMain.updateAll(whereData, oloOrderMainData, function (err, _oloOrderMain) {});
					//for update cancel reason.

					//for get order id.
					let sql = 'SELECT * FROM `olo_order_main` WHERE `deliveryId` = "' + deliveryId + '" LIMIT 1';
					Deliveries.dataSource.connector.query(sql, (err, dataResults) => {
						let op = {
							"confirmationId": dataResults[0]["order_id"],
							"success": true,
							"message": "Delivery successfully canceled"
						}
						cb(err, op)
					});
					//for get order id.
				}else{
					cb(null,null)
				}
			}

		});


	};
//----------------------------/deliveries/deliveryId/cancel---------------------------------------//

//----------------------------/deliveries/deliveryId/addtip---------------------------------------//
	Deliveries.remoteMethod(
		'deliveriesAddtip',
		{
			description: 'for delivery cancel.',
			http: { path: '/:deliveryId/cancel', verb: 'post' },
			accepts: [
				{ arg: 'deliveryId', type: 'string', http: { source: 'path' } },
				{ arg: 'data', type: 'object', http: { source: 'body' } }
			],
			returns: { type: 'array', root: true }
		}
	);

	Deliveries.deliveriesAddtip = function (deliveryId, data, cb) {
		let sql = 'UPDATE `order_main` SET `order_status` = "cancelled" WHERE `order_id` =(SELECT `order_id` FROM `olo_order_main` WHERE `deliveryId` = "' + deliveryId + '" LIMIT 1)';

		Deliveries.dataSource.connector.query(sql, (err, dataResults) => {
			if (err) {
				cb(null, err);
			} else {
				if (dataResults["affectedRows"] > 0) {

				} else {
					cb(null, null)
				}
			}

		});


	};
//----------------------------/deliveries/deliveryId/addtip---------------------------------------//

};
