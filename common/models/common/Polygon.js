
module.exports = class Polygon {
 
    pointInPolygon(point, polygon, pointOnVertex = true) {
        var varpointOnVertex = pointOnVertex;
 
        // Transform string coordinates into arrays with x and y values
        point = this.pointStringToCoordinates(point);
        var vertices = []; 
        polygon = polygon.split("#");
        var item = "";
        for (var v = 0; v < polygon.length; v++) {
        	item = polygon[v];
        	if(item != ""){
                vertices.push(this.pointStringToCoordinates(item));
            }
        }
 
        // Check if the point sits exactly on a vertex
        if (varpointOnVertex == true && this.pointOnVertex(point, vertices) == true) {
            return "vertex";
        }
 
        // Check if the point is inside the polygon or on the boundary
        var intersections = 0; 
        var vertices_count = vertices.length;
 		var vertex1 = ""; 
        var vertex2 = "";
        var i = 0;
        var xinters = "";
        for (i=1; i < vertices_count; i++) {
            vertex1 = vertices[i-1]; 
            vertex2 = vertices[i];
            if (vertex1['y'] == vertex2['y'] && vertex1['y'] == point['y'] && point['x'] > Math.min(vertex1['x'], vertex2['x']) && point['x'] < Math.max(vertex1['x'], vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                return "boundary";
            }
            if (point['y'] > Math.min(vertex1['y'], vertex2['y']) && point['y'] <= Math.max(vertex1['y'], vertex2['y']) && point['x'] <= Math.max(vertex1['x'], vertex2['x']) && vertex1['y'] != vertex2['y']) { 
                xinters = (point['y'] - vertex1['y']) * (vertex2['x'] - vertex1['x']) / (vertex2['y'] - vertex1['y']) + vertex1['x']; 
                if (xinters == point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return "boundary";
                }
                if (vertex1['x'] == vertex2['x'] || point['x'] <= xinters) {
                    intersections++; 
                }
            } 
        } 
        // If the number of edges we passed through is odd, then it's in the polygon. 
        if (intersections % 2 != 0) {
            return "inside";
        } else {
            return "outside";
        }
    }
 
    pointOnVertex(point, vertices) {
        for(let i=0;i<vertices.length;i++) {
            if (point == vertices[i]) {
                return true;
            }
        }
    }
 
    pointStringToCoordinates(pointString) {
        var coordinates = pointString.split(",");
        return {x: Number(coordinates[0]), y: Number(coordinates[1])};
    };

}
